﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models.Customer;

namespace Vidly.Models.ViewModels.Customer
{
    public class CustomerViewModel
    {
        public IEnumerable<CustomerData> Items { get; set; }

        public CustomerSearchData Search { get; set; }

        public CustomerData Item { get; set; }
    }
}