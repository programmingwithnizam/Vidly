﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Vidly.Models.Movie;

namespace Vidly.Models.ViewModels.Movie
{
    public class MovieViewModel
    {
        public IEnumerable<MovieData> Items { get; set; }

        public MovieSearchData Search { get; set; }

        public MovieData Item { get; set; }
    }
}