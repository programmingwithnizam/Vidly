﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Models.Movie
{
    public class MovieSearchData
    {
        public Guid? MovieId { get; set; }
    }
}