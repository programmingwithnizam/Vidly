﻿using System;
using Dapper;
using System.Collections.Generic;

namespace Vidly.Models.Movie
{
    public class Movie
    {
        public Guid MovieId { get; set; }
        public string MovieName { get; set; }

        public DateTime ReleaseDate { get; set; }

        public DateTime DateAdded { get; set; }

        public int NumberInStock { get; set; }

        public static IEnumerable<Movie> Get()
        {
            return VidlyDB.Instance.DB.GetList<Movie>();
        }
    }
}