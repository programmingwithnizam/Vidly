﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Vidly.Models.Movie
{
    public class MovieData
    {
        public Guid MovieId { get; set; }
        public string MovieName { get; set; }

        public DateTime ReleaseDate { get; set; }

        public DateTime DateAdded { get; set; }

        public int NumberInStock { get; set; }

        public string GenreName { get; set; }
    }
}