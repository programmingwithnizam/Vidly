﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Models.Movie
{
    public class Genre
    {
        public Guid GenreId { get; set; }

        public string GenreName { get; set; }
    }
}