﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;

namespace Vidly.Models.Customer
{
    public class MembershipType
    {
        public int MembershipTypeId { get; set; }

        public string Name { get; set; }

        public static MembershipType Get(Guid membershipTypeId)
        {
            var sql = @"SELECT 
                            MembershipTypeId,
                            Name
                        FROM MembershipType
                        WHERE MembershipTypeId = @MembershipTypeId";
            return VidlyDB.Instance.DB.QueryFirst<MembershipType>(sql, new { MembershipTypeId = membershipTypeId });
        }
    }
}