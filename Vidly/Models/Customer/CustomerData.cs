﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.AccessControl;
using System.Web;

namespace Vidly.Models.Customer
{
    public class CustomerData
    {
        public Guid CustomerId { get; set; }

        public string CustomerName { get; set; }

        public Guid MembershipTypeId { get; set; }

        public string MembershipName { get; set; }

        public int DiscountRate { get; set; }

        public int SignUpFee { get; set; }

        public DateTime? BirthDate { get; set; }
    }
}