﻿using System;
using Dapper;
using System.Collections.Generic;
using System.Data.Entity.Core.EntityClient;

namespace Vidly.Models.Customer
{
    public class Customer
    {
        public Guid CustomerId { get; set; }
        public string CustomerName { get; set; }

        public Guid MembershipTypeId { get; set; }

        public static Customer Get(Guid customerId)
        {
            var sql = @"SELECT CustomerName, 
                            MembershipTypeId
                        FROM Customer
                        WHERE CustomerId = @customerId";
            return VidlyDB.Instance.DB.QueryFirst<Customer>(sql, new { id = customerId });
        }
    }
}