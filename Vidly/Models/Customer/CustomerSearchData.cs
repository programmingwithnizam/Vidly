﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Vidly.Models.Customer
{
    public class CustomerSearchData
    {
        public Guid? CustomerId { get; set; }
    }
}