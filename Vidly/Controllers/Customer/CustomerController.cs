﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Managers;
using Vidly.Models;
using Vidly.Models.Customer;
using Vidly.Models.ViewModels.Customer;

namespace Vidly.Controllers.Customer
{
    public class CustomerController : Controller
    {
        CustomerManager customerMgr = new CustomerManager();
        // GET: Customers
        public ActionResult Index()
        {
            var model = new CustomerViewModel();
            model.Search = new CustomerSearchData();

            model.Items = customerMgr.SearchCustomers(model.Search).ToList();
            return View(model);
        }

        public ActionResult Details(Guid? customerId)
        {
            var model = new CustomerViewModel();
            model.Search = new CustomerSearchData();

            model.Search.CustomerId = customerId;
            model.Item = customerMgr.SearchCustomers(model.Search).FirstOrDefault();

            if (model.Item == null)
                return HttpNotFound();

            return View(model);
        }
    }
}