﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vidly.Managers;
using Vidly.Models.Movie;
using Vidly.Models.ViewModels.Movie;

namespace Vidly.Controllers
{
    public class MovieController : Controller
    {
        MovieManager movieMgr = new MovieManager();
        // GET: Movie
        public ActionResult Index()
        {
            var model = new MovieViewModel();
            model.Search = new MovieSearchData();

            model.Items = movieMgr.SearchMovies(model.Search).ToList();
            return View(model);
        }

        public ActionResult Details(Guid? MovieId)
        {
            var model = new MovieViewModel();
            model.Search = new MovieSearchData();

            model.Search.MovieId = MovieId;

            model.Item = movieMgr.SearchMovies(model.Search).FirstOrDefault();
            if (model.Item == null)
                return HttpNotFound();

            return View(model);
        }
    }
}