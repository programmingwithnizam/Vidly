﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using Vidly.Models;
using Vidly.Models.Customer;

namespace Vidly.Managers
{
    public class CustomerManager
    {
        public IEnumerable<CustomerData> SearchCustomers(CustomerSearchData search)
        {
            var builder = new SqlBuilder();
           

            var sql = builder.AddTemplate(@"
                SELECT CustomerId,
                    CustomerName,
                    IsSubscribedToNewsLetter,
                    MembershipType.MembershipTypeId,
                    MembershipType.Name [MembershipName],
                    MembershipType.DiscountRate,
                    MembershipType.SignUpFee,
                    BirthDate
                FROM Customer
                JOIN MembershipType ON MembershipType.MembershipTypeId = Customer.MembershipTypeId
                /**where**/");

            if(search.CustomerId != null)
                builder.Where("Customer.CustomerId = @CustomerId", new { CustomerId = search.CustomerId });

            return VidlyDB.Instance.DB.Query<CustomerData>(sql.RawSql, sql.Parameters);
        }
    }
}