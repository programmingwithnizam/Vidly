﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using Vidly.Models;
using Vidly.Models.Movie;

namespace Vidly.Managers
{
    public class MovieManager
    {
        public IEnumerable<MovieData> SearchMovies(MovieSearchData search)
        {
            var builder = new SqlBuilder();

            var sql = builder.AddTemplate(@"
                SELECT MovieId,
                    MovieName,
                    ReleaseDate,
                    DateAdded,
                    NumberInStock,
                    Genre.GenreId,
                    Genre.GenreName
                FROM Movie
                JOIN Genre ON Genre.GenreId = Movie.GenreId
                /**where**/");

            if (search.MovieId != null)
                builder.Where("Movie.MovieId = @MovieId", new { MovieId = search.MovieId });

            return VidlyDB.Instance.DB.Query<MovieData>(sql.RawSql, sql.Parameters);
        }
    }
}