﻿using System.Configuration;
using System.Data;
using System.Data.Common;

namespace Vidly.Models
{
    public class VidlyDB
    {
        #region Singleton
        private static VidlyDB _instance;

        private VidlyDB()
        {
        }

        public static VidlyDB Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new VidlyDB();
                }
                return _instance;
            }
        }
        #endregion

        public IDbConnection DB
        {
            get { return Open(); }
        }

        private IDbConnection Open()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["VidlyConnection"];
            var factory = DbProviderFactories.GetFactory(connectionString.ProviderName);
            var db = factory.CreateConnection();
            db.ConnectionString = connectionString.ConnectionString;

            return db;
        }
    }
}